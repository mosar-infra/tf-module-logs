# logs/main.tf

resource "aws_cloudwatch_log_group" "log_group" {
  name = var.log_group_name

  tags = {
    Environment = var.environment
    Name = var.log_group_name
  }
}
